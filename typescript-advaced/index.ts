interface Person<T> {
  name: string;
  something: T;

  introduce(): void;
}

abstract class PersonImpl implements Person<string> {
  name: string;
  something: string;

  constructor(name: string) {
    this.name = name;
  }
  introduce(): void {
    console.log(this.name);
  }
}
