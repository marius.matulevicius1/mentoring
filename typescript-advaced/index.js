// creates a self invoking function that creates a person implementation object
// Then adds the required methods to the object
// It seems that the actual interface or generics are not reflected in the js code
// It only compiles the code according to generics and interfaces

var PersonImpl = /** @class */ (function () {
  function PersonImpl(name) {
    this.name = name;
  }
  PersonImpl.prototype.introduce = function () {
    console.log(this.name);
  };
  return PersonImpl;
})();
