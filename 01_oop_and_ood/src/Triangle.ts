import { Point } from './Point';
import { Shape } from './Shape';

export class Triangle extends Shape {
  pointA: Point;
  pointB: Point;
  pointC: Point;
  constructor(pointA: Point, pointB: Point, pointC: Point, color?: string, filled?: boolean) {
    super([pointA, pointB, pointC], color, filled);
    this.pointA = pointA;
    this.pointB = pointB;
    this.pointC = pointC;
  }

  toString(): string {
    return `Triangle[v1=${this.pointA.toString()},v2=${this.pointB.toString()},v3=${this.pointC.toString()}]`;
  }

  getType(): string {
    // Just have a question here: Why am I unable to access points variable from super class using
    // super.points
    const sideLengthAB = this.pointA.distance(this.pointB).toFixed(2);
    const sideLengthBC = this.pointB.distance(this.pointC).toFixed(2);
    const sideLengthCA = this.pointC.distance(this.pointA).toFixed(2);

    if (sideLengthAB === sideLengthBC && sideLengthBC === sideLengthCA && sideLengthCA === sideLengthAB) {
      return 'equilateral triangle';
    } else if (sideLengthAB === sideLengthBC || sideLengthBC === sideLengthCA || sideLengthCA === sideLengthAB) {
      return 'isosceles triangle';
    } else {
      return 'scalene triangle';
    }
  }
}
