export class Point {
  x: number;
  y: number;

  constructor();
  constructor(x: number, y: number);
  constructor(...args: number[]) {
    if (args.length) {
      this.x = args[0];
      this.y = args[1];
    } else {
      this.x = 0;
      this.y = 0;
    }
  }

  toString(): string {
    return `(${this.x}, ${this.y})`;
  }

  distance(otherPoint: Point): number;
  distance(x: number, y: number): number;
  distance(...args): number {
    let x2;
    let y2;
    if (args.length === 2) {
      x2 = args[0];
      y2 = args[1];
    } else if (args.length === 1) {
      x2 = args[0].x;
      y2 = args[0].y;
    } else {
      x2 = 0;
      y2 = 0;
    }
    return Math.sqrt(Math.pow(x2 - this.x, 2) + Math.pow(y2 - this.y, 2));
  }
}
