import { Point } from './Point';

export abstract class Shape {
  protected color: string;
  protected filled: boolean;
  points: Array<Point>;

  constructor(points: Array<Point>);
  constructor(points: Array<Point>, color: string, filled: boolean);
  constructor(...args: any[]) {
    if (args[0].length < 3) throw new Error('invalid amount of points');
    if (args.length === 1) {
      this.color = 'green';
      this.filled = true;
      this.points = args[0];
    } else if (args.length === 3) {
      this.points = args[0];
      this.color = args[1];
      this.filled = args[2];
    }
  }

  toString(): string {
    const isFilledText = this.filled ? 'filled' : 'not filled';
    const pointsString = this.points.map((point) => point.toString(), []).join(', ');
    return `A Shape with color of ${this.color} and ${isFilledText}. Points: ${pointsString}.`;
  }

  getPerimeter() {
    return this.points.reduce((acc, point, index) => {
      const nextPoint = this.points[index + 1];
      if (nextPoint) {
        acc += point.distance(nextPoint);
      } else {
        const firstPoint = this.points[0];
        acc += point.distance(firstPoint);
      }
      return acc;
    }, 0);
  }
  abstract getType(): string;
}
