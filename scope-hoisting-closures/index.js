// 1. Create a function runningAverage() that returns a callable function object. Update the
// series with each given value and calculate the current average.

const runningAverage = () => {
  let sum = 0;
  let numberOfNumbers = 0;
  return (number) => {
    numberOfNumbers++;
    sum += number;
    return sum / numberOfNumbers;
  };
};

rAvg = runningAverage();

console.log(rAvg(10));
console.log(rAvg(11));
console.log(rAvg(12));
console.log('----------------------------------------------------------------');

// 2. Write a sum function which will work properly when invoked using syntax below.

const sumNumbers = (arr) => arr.reduce((a, b) => a + b, 0);

const calculateSum = () => {
  let totalSum = 0;

  const fn = (...number) => {
    if (number.length) {
      totalSum = sumNumbers([...number, totalSum]);

      console.log(totalSum);

      return fn;
    }

    return totalSum;
  };

  return fn;
};

const countFn = calculateSum();

countFn(10, 23)(12)(13);
