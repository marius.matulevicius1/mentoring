class RandomError extends Error {
  constructor(message) {
    super(message);
    this.name = 'RandomError';
  }
}

function test() {
  throw new RandomError('Whoops!');
}

try {
  test();
} catch (err) {
  console.log(err.message);
  console.log(err.name);
  console.log(err.stack);
}
