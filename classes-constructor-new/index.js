class Vector {
  constructor(components) {
    this.components = components;
  }

  isSameLength(b) {
    return this.components.length === b.components.length;
  }

  add(b) {
    this.#checkAndThrowNotSameVectorLengthError(b);

    let sums = [];

    for (let i = 0, length = this.components.length; i < length; i++) {
      sums.push(this.components[i] + b.components[i]);
    }

    return new Vector(sums);
  }

  subtract(b) {
    this.#checkAndThrowNotSameVectorLengthError(b);

    let differences = [];

    for (let i = 0, length = this.components.length; i < length; i++) {
      differences.push(this.components[i] - b.components[i]);
    }

    return new Vector(differences);
  }

  dot(b) {
    this.#checkAndThrowNotSameVectorLengthError(b);

    return this.components.reduce((a, item, i) => a + item * b.components[i], 0);
  }

  norm() {
    return Math.sqrt(this.components.reduce((a, b) => a + Math.pow(b, 2)));
  }

  equals(b) {
    if (!this.isSameLength(b)) {
      return false;
    }

    for (let i = 0; i < this.components.length; i++) {
      if (this.components[i] !== b.components[i]) {
        return false;
      }
    }

    return true;
  }

  toString() {
    return '(' + this.components.join(',') + ')';
  }

  #checkAndThrowNotSameVectorLengthError(b) {
    if (!this.isSameLength(b)) {
      throw Error('Vectors must be the same length.');
    }
  }
}

const a = new Vector([1, 2, 3]);
const b = new Vector([3, 4, 5]);
const c = new Vector([5, 6, 7, 8]);

a.subtract(b);
a.dot(b);
a.norm();
a.add(c);
a.toString();
