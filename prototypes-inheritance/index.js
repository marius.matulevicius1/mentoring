function myNew(constructor, ...args) {
  const newObject = Object.create(constructor.prototype);
  constructor.apply(newObject, args);

  return newObject;
}

function Person(name, age) {
  this.name = name;
  this.age = age;
}

Person.prototype.introduce = function () {
  return 'My name is ' + this.name + ' and I am ' + this.age;
};

const myJohn = myNew(Person, 'John', 30);
const john = new Person('John', 30);
const jack = new Person('Jack', 40);
console.log(myJohn.introduce()); // My name is John and I am 30
console.log(john.introduce()); // My name is John and I am 30
console.log(jack.introduce()); // My name is Jack and I am 40
