class ReversePromise {
  constructor(executorFn) {
    this.fulfilmentHandlers = [];
    executorFn(this.resolve.bind(this));
  }

  resolve() {
    setTimeout(() => {
      for (let i = this.fulfilmentHandlers.length - 1; i >= 0; i--) {
        this.fulfilmentHandlers[i]();
      }
    }, 0);
  }

  then(fulfilmentHandler) {
    this.fulfilmentHandlers.push(fulfilmentHandler);
    return this;
  }
}

let revPromise = new ReversePromise((resolve) => {
  console.log(1);
  resolve();
})
  .then(() => console.log(2))
  .then(() => console.log(3))
  .then(() => console.log(4));

// 1 4 3 2
